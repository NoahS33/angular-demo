import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Alert, AlertService } from '../alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit, OnDestroy {
  subs: Subscription
  alert: Alert
  staticAlertClosed = true
  constructor(private alertService: AlertService) { }
  ngOnDestroy(): void {
    this.subs?.unsubscribe()
  }

  ngOnInit(): void {
    this.subs = this.alertService.alert$.subscribe((alert) =>{
      this.alert = alert
      this.staticAlertClosed =false;

      setTimeout(() => (this.staticAlertClosed =true), 3000)
    })
  }

}
