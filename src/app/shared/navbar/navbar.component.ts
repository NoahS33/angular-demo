import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Authservice } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  token: string;
  @Input() title: string;
  isNavbarCollapsed = true;

  constructor(private authService: Authservice,
    private route: ActivatedRoute,
    private router: Router) {}

  ngOnInit(): void {
    this.token = localStorage.getItem("token")
  }
  logout(){
    this.authService.logout();
        this.router.navigate(['/login']);
  }

}
