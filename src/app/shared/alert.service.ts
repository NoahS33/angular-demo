import { Injectable, Input } from '@angular/core';
import { Subject } from 'rxjs';
export interface Alert {
  type: string
  message:string
}

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  public alert$ = new Subject<Alert>()

  succes(msg: string):void{

    this.alert$.next({type: 'succes', message: msg})
  }

  error(msg: string):void{
    console.log("error alert service is aangeroepen")
    this.alert$.next({type: 'error', message: msg})
  }
}
