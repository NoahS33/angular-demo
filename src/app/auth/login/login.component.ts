import { Component, OnInit } from "@angular/core"
import { error } from '@angular/compiler/src/util';
import { first, map } from 'rxjs/operators';
import { Router } from "@angular/router"
import { NgForm } from "@angular/forms"
import { Authservice } from "../auth.service"

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  alert = false
  constructor(private router: Router, public authService: Authservice) {}

  ngOnInit(): void {}

  login(form: NgForm): void {
    if (form.invalid) {
      return
    }
    this.authService.login(
      form.value.email,
      form.value.password)

    console.log(form.value)

    this.router.navigate(["/dashboard"])
  }
}
