import { Component, OnInit } from "@angular/core"
import { Router } from "@angular/router"
import { NgForm } from "@angular/forms"
import { Authservice } from "../auth.service"

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  constructor(private router: Router, public authService: Authservice) {}

  ngOnInit(): void {}

  register(form: NgForm): void {
    if (form.invalid) {
      return
    }
    this.authService.createUser(
      form.value.firstname,
      form.value.lastname,
      form.value.email,
      form.value.password
    )
    this.router.navigate(["/login"])
  }
}
