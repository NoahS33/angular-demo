import { HttpClient } from "@angular/common/http"
import { Injectable } from "@angular/core"
import { Router } from "@angular/router"
import { Subject } from "rxjs"
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { environment } from "src/environments/environment"
import { catchError, map, tap } from 'rxjs/operators';
import { idText } from "typescript"
import { User } from "../pages/user/user.model"
import { AlertService } from '../shared/alert.service'

@Injectable({ providedIn: "root" })
export class Authservice {
  private token: string;
  private user: User;
  private isAuthenticated: boolean;
  authStatusListener$ = new Subject<boolean>();

  constructor(private http: HttpClient, private router: Router, private alertService: AlertService) {}

  createUser(
    firstname: string,
    lastname: string,
    email: string,
    password: string
  ) {
    const user: User = {
      id: "",
      firstName: firstname,
      lastName: lastname,
      emailAdress: email,
      password: password
    }
    console.log(user)
    console.log(environment.api_url + "/users/signup")
    this.http
      .post(environment.api_url + "/users/signup", user)
      .subscribe((response) => {
          this.alertService.succes('Je bent nu geregistreerd')
        }, err => {
          this.alertService.error("Dit emailadres heeft al een account!")
          return (undefined)
    })

      }

  login(emailaddress: string, password: string) {
    const authData = {
      email: emailaddress,
      password: password
    };
    this.http.post<{ token: string, expiresIn: number, user: User }>(environment.api_url + '/users/login', authData)
      .subscribe(response => {
        this.alertService.succes('You have been logged in')
        console.log(response);
        const expiresInDuration = response.expiresIn;

        this.token = response.token;
        this.isAuthenticated = true;
        this.authStatusListener$.next(true);

        // const now = new Date();
        // const expirationDate = new Date(now.getTime() + expiresInDuration * 1000);
        this.saveAuthData(response.token);

        this.router.navigate(['/']);
      }, err => {
        this.alertService.succes('Wachtwoord of emailadres zijn incorrect!')
      })

  }

  private saveAuthData(token: string) {
    localStorage.setItem('token', token);
    localStorage.setItem('isAuthenticated', this.isAuthenticated.toString());
  }

  public getToken(): string {
    return localStorage.getItem("token");
  }

  public logout(): void {
    console.log("logout() is aangeroepen");
    localStorage.removeItem("token");
  }


}
