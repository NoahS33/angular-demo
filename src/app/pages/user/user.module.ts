import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import * as fromComponents from '.';
import { AuthGuardGuard } from 'src/app/auth/auth-guard.guard';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: fromComponents.UserListComponent, canActivate: [AuthGuardGuard] },
  {
    path: ':id',
    pathMatch: 'full',
    component: fromComponents.UserDetailComponent, canActivate: [AuthGuardGuard],
  },
];

@NgModule({
  declarations: [...fromComponents.components],
  imports: [CommonModule, RouterModule.forChild(routes)],
})
export class UserModule {}
