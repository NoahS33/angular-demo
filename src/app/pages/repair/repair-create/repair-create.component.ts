import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from "@angular/router"
import { Repair } from "../repair.model"
import { RepairService } from "../repair.service"
import { tap, map, switchMap, catchError } from "rxjs/operators"
import { Observable, of, Subscription } from "rxjs"

@Component({
  selector: 'app-repair-create',
  templateUrl: './repair-create.component.html',
  styleUrls: ['./repair-create.component.css']
})
export class RepairCreateComponent implements OnInit {
  title: string
  // editMode switches between editing existing user or creating a new user.
  // Default is false (= create new user)
  editMode: boolean
  id: string
  repair: Repair
  submitted = false
  subscribtion: Subscription
  deletesubscribtion: Subscription
  customerId: string
  payterminalId: string
  routeSub: Subscription
  payterminalRouteSub: Subscription

  constructor(
    private repairService: RepairService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnDestroy(): void {
    this.subscribtion?.unsubscribe()
    this.deletesubscribtion?.unsubscribe()
    this.routeSub?.unsubscribe()
    this.payterminalRouteSub?.unsubscribe()
  }

  ngOnInit(): any {
    this.subscribtion = this.route.paramMap
    .pipe(
      tap(console.log),
      switchMap((params: ParamMap) => {

          return of (
            new Repair({
            title: "",
            description: "",
            date: ""
          }))

      })

    )
    .subscribe(repair =>
        this.repair = repair
    )
  }

  onSubmit() {
    this.routeSub = this.route.params.subscribe(params => this.customerId = params["customerId"])
    this.payterminalRouteSub = this.route.params.subscribe(params => this.payterminalId = params["payterminalId"])

    this.submitted = true

    // Save user via the service
    // Then navigate back to display view (= UserDetails).
    // The display view must then show the new or edited user.

    console.log("onSubmit: ", this.repair)

    this.repairService
      .create(this.customerId, this.payterminalId, this.repair)
      .pipe(tap((data) => console.log("response:", data)))
      .subscribe(
        (data) => console.log(data),
        (error) => console.error(error)
      )

    this.router.navigate([`/customers`])
  }


}
