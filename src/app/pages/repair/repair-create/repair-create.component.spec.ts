import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairCreateComponent } from './repair-create.component';

describe('RepairCreateComponent', () => {
  let component: RepairCreateComponent;
  let fixture: ComponentFixture<RepairCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepairCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RepairCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
