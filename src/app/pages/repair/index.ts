import { RepairDetailComponent } from "./repair-detail/repair-detail.component"
import { RepairCreateComponent } from "./repair-create/repair-create.component"
import { RepairUpdateComponent } from "./repair-update/repair-update.component"

export const components: any[] = [
  RepairDetailComponent,
  RepairCreateComponent,
  RepairUpdateComponent
]

export * from "./repair-detail/repair-detail.component"
export * from "./repair-create/repair-create.component"
export * from "./repair-update/repair-update.component"
