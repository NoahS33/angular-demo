import { Injectable } from "@angular/core"
import { from, Observable, of } from "rxjs"
import { delay, filter, map, mergeMap, take } from "rxjs/operators"
import { Repair } from "./repair.model"
import { HttpClient, HttpErrorResponse } from "@angular/common/http"
import { environment } from "src/environments/environment"


@Injectable({
  providedIn: 'root'
})
export class RepairService {
  token: string = localStorage.getItem("token")
  constructor(private http: HttpClient) {}


  getById(customerId: string, payterminalId: string, id: string): Observable<Repair> {
    return this.http.get<any>(`${environment.api_url}/customers/${customerId}/payterminals/${payterminalId}/repairs/${id}`).pipe()

  }

  delete(customerId: string, payterminalId:string, id: string) {
    return this.http.delete<any>(`${environment.api_url}/customers/${customerId}/payterminals/${payterminalId}/repairs/${id}`)
  }

  update(customerId: string, payterminalId: string, repair: Repair) {
    return this.http.put<any>(
      `${environment.api_url}/customers/${customerId}/payterminals/${payterminalId}/repairs/${repair._id}`,
      repair
    )
  }
  create(customerId:string, payterminalId: string, repair: Repair) {
    return this.http.post<any>(
      `${environment.api_url}/customers/${customerId}/payterminals/${payterminalId}/repairs`,
      repair
    )
  }

}
