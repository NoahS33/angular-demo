export class Repair {
  _id: string
  title: string
  description: string
  date: Date
  constructor(values: Object = {}) {
    Object.assign(this, values)
  }
}
