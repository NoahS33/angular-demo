import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from "@angular/router"
import { Repair } from "../repair.model"
import { RepairService } from "../repair.service"
import { tap, map, switchMap, catchError } from "rxjs/operators"
import { Observable, Subscription } from "rxjs"

@Component({
  selector: 'app-repair-update',
  templateUrl: './repair-update.component.html',
  styleUrls: ['./repair-update.component.css']
})
export class RepairUpdateComponent implements OnInit {
  title: string
  // editMode switches between editing existing user or creating a new user.
  // Default is false (= create new user)
  editMode: boolean
  id: string
  repair: Repair
  submitted = false
  subscribtion: Subscription
  deletesubscribtion: Subscription
  customerId: string
  payterminalId: string
  routeSub: Subscription
  payterminalRouteSub

  constructor(
    private repairService: RepairService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnDestroy(): void {
    this.subscribtion?.unsubscribe()
    this.deletesubscribtion?.unsubscribe()
    this.routeSub?.unsubscribe()
    this.payterminalRouteSub?.unsubscribe()
  }


  ngOnInit() {
    this.subscribtion = this.route.paramMap
    .pipe(
      switchMap((params: ParamMap) =>
        this.repairService.getById(params.get("customerId"), params.get("payterminalId"), params.get("id"))
      ),
    )
      .subscribe(
        (repair) => {
          // make local copy of payterminal - detached from original array
          this.repair = new Repair(repair)
          console.log(repair)
        },
        (err) => console.log("An error ocurred:", err)
      )
  }

  onSubmit() {
    this.routeSub = this.route.params.subscribe(params => this.customerId = params["customerId"])
    this.payterminalRouteSub = this.route.params.subscribe(params => this.payterminalId = params["payterminalId"])

    this.submitted = true

    // Save user via the service
    // Then navigate back to display view (= UserDetails).
    // The display view must then show the new or edited user.

    console.log("onSubmit: ", this.repair)

    this.repairService
      .update(this.customerId, this.payterminalId, this.repair)
      .pipe(tap((data) => console.log("response:", data)))
      .subscribe(
        (data) => console.log(data),
        (error) => console.error(error)
      )

    this.router.navigate([`/customers`])
  }

}
