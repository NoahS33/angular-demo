import { Component, OnDestroy, OnInit } from "@angular/core"
import { CommonModule } from "@angular/common"
import { Router } from "@angular/router"
import { ActivatedRoute, ParamMap } from "@angular/router"
import { Observable, Subscription } from "rxjs"
import { switchMap, tap } from "rxjs/operators"
import { Repair } from "../repair.model"
import { RepairService } from "../repair.service"

@Component({
  selector: 'app-repair-detail',
  templateUrl: './repair-detail.component.html',
  styleUrls: ['./repair-detail.component.css']
})
export class RepairDetailComponent implements OnInit {
  title = "Payterminal DetailComponent"
  repair: Repair
  subscribtion: Subscription
  deletesubscribtion: Subscription
  customerId: string
  payterminalId: string
  routeSub: Subscription
  payterminalRouteSub: Subscription

  constructor(
    private router: Router,
    private repairService: RepairService,
    private route: ActivatedRoute
  ) {}

  ngOnDestroy(): void {
    this.subscribtion?.unsubscribe()
    this.deletesubscribtion?.unsubscribe()
    this.routeSub?.unsubscribe()
    this.payterminalRouteSub?.unsubscribe()
  }

  ngOnInit() {
    this.subscribtion = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.repairService.getById(params.get("customerId"), params.get("payterminalId"), params.get("id"))
        )
      )
      .subscribe((repair) => {
        this.repair = repair
        console.log(repair)
      })
  }

  delete() {
    this.routeSub = this.route.params.subscribe(params => this.customerId = params["customerId"])
    this.routeSub = this.route.params.subscribe(params => this.payterminalId = params["payterminalId"])
    this.deletesubscribtion = this.repairService
      .delete(this.customerId, this.payterminalId, this.repair._id)
      .subscribe(() => {
        this.router.navigate(["/customers"])
      })
  }

}
