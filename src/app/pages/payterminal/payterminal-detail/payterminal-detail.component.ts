import { Component, OnDestroy, OnInit } from "@angular/core"
import { CommonModule } from "@angular/common"
import { Router } from "@angular/router"
import { ActivatedRoute, ParamMap } from "@angular/router"
import { Observable, Subscription } from "rxjs"
import { switchMap, tap } from "rxjs/operators"
import { Payterminal } from "../payterminal.model"
import { PayterminalService } from "../payterminal.service"

@Component({
  selector: "app-payterminal-detail",
  templateUrl: "./payterminal-detail.component.html",
  styleUrls: ["./payterminal-detail.component.css"]
})
export class PayterminalDetailComponent implements OnInit {
  title = "Payterminal DetailComponent"
  payterminal: Payterminal
  subscribtion: Subscription
  deletesubscribtion: Subscription
  customerId: string
  routeSub: Subscription

  constructor(
    private router: Router,
    private payterminalService: PayterminalService,
    private route: ActivatedRoute
  ) {}

  ngOnDestroy(): void {
    this.subscribtion?.unsubscribe()
    this.deletesubscribtion?.unsubscribe()
    this.routeSub?.unsubscribe()
  }

  ngOnInit() {
    this.subscribtion = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.payterminalService.getById(params.get("customerId"), params.get("id"))
        )
      )
      .subscribe((payterminal) => {
        this.payterminal = payterminal
        console.log(payterminal)
      })
  }

  delete() {
    this.routeSub = this.route.params.subscribe(params => this.customerId = params["customerId"])
    this.deletesubscribtion = this.payterminalService
      .delete(this.customerId, this.payterminal._id)
      .subscribe(() => {
        this.router.navigate(["/customers"])
      })
  }
}
