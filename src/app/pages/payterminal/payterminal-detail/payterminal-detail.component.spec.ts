import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PayterminalDetailComponent } from './payterminal-detail.component';

describe('PayterminalDetailComponent', () => {
  let component: PayterminalDetailComponent;
  let fixture: ComponentFixture<PayterminalDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PayterminalDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PayterminalDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
