import { TestBed } from '@angular/core/testing';

import { PayterminalService } from './payterminal.service';

describe('PayterminalService', () => {
  let service: PayterminalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PayterminalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
