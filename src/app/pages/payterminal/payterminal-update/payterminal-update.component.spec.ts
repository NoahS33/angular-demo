import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PayterminalUpdateComponent } from './payterminal-update.component';

describe('PayterminalUpdateComponent', () => {
  let component: PayterminalUpdateComponent;
  let fixture: ComponentFixture<PayterminalUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PayterminalUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PayterminalUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
