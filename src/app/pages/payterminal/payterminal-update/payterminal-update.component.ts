import { Component, OnDestroy, OnInit } from "@angular/core"
import { ActivatedRoute, ParamMap, Router } from "@angular/router"
import { Payterminal } from "../payterminal.model"
import { PayterminalService } from "../payterminal.service"
import { tap, map, switchMap, catchError } from "rxjs/operators"
import { Observable, Subscription } from "rxjs"

@Component({
  selector: "app-payterminal-update",
  templateUrl: "./payterminal-update.component.html",
  styleUrls: ["./payterminal-update.component.css"]
})
export class PayterminalUpdateComponent implements OnInit {
  title: string
  // editMode switches between editing existing user or creating a new user.
  // Default is false (= create new user)
  editMode: boolean
  id: string
  payterminal: Payterminal
  submitted = false
  subscribtion: Subscription
  deletesubscribtion: Subscription
  customerId: string
  routeSub: Subscription

  condition = [
    { gebruikt: "Nieuw" },
    { gebruikt: "Overgezet" },
    { gebruikt: "Tweedehands" }
  ]

  constructor(
    private payterminalService: PayterminalService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnDestroy(): void {
    this.subscribtion?.unsubscribe()
    this.deletesubscribtion?.unsubscribe()
    this.routeSub?.unsubscribe()
  }

  ngOnInit() {
    this.subscribtion = this.route.paramMap
    .pipe(
      switchMap((params: ParamMap) =>
        this.payterminalService.getById(params.get("customerId"), params.get("id"))
      ),
    )
      .subscribe(
        (payterminal) => {
          // make local copy of payterminal - detached from original array
          this.payterminal = new Payterminal(payterminal)
        },
        (err) => console.log("An error ocurred:", err)
      )
  }

  onSubmit() {
    this.routeSub = this.route.params.subscribe(params => this.customerId = params["customerId"])
    this.submitted = true

    // Save user via the service
    // Then navigate back to display view (= UserDetails).
    // The display view must then show the new or edited user.

    console.log("onSubmit: ", this.payterminal)

    this.payterminalService
      .update(this.customerId, this.payterminal)
      .pipe(tap((data) => console.log("response:", data)))
      .subscribe(
        (data) => console.log(data),
        (error) => console.error(error)
      )

    this.router.navigate([`/customers`])
  }


}
