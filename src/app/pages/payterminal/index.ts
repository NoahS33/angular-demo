import { PayterminalListComponent } from "./payterminal-list/payterminal-list.component"
import { PayterminalDetailComponent } from "./payterminal-detail/payterminal-detail.component"
import { PayterminalCreateComponent } from "./payterminal-create/payterminal-create.component"
import { PayterminalUpdateComponent } from "./payterminal-update/payterminal-update.component"

export const components: any[] = [
  PayterminalListComponent,
  PayterminalDetailComponent,
  PayterminalCreateComponent,
  PayterminalUpdateComponent
]

export * from "./payterminal-list/payterminal-list.component"
export * from "./payterminal-detail/payterminal-detail.component"
export * from "./payterminal-create/payterminal-create.component"
export * from "./payterminal-update/payterminal-update.component"
