import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { RouterModule, Routes } from "@angular/router"
import * as fromComponents from "."
import { FormsModule, ReactiveFormsModule } from "@angular/forms"
import { AuthGuardGuard } from "src/app/auth/auth-guard.guard"

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    component: fromComponents.PayterminalListComponent, canActivate: [AuthGuardGuard]
  },
  {
    path: "create",
    pathMatch: "full",
    component: fromComponents.PayterminalCreateComponent, canActivate: [AuthGuardGuard]
  },
  {
    path: ":id",
    pathMatch: "full",
    component: fromComponents.PayterminalDetailComponent, canActivate: [AuthGuardGuard]
  },
  {
    path: ":id/update",
    pathMatch: "full",
    component: fromComponents.PayterminalUpdateComponent, canActivate: [AuthGuardGuard]
  }
]

@NgModule({
  declarations: [...fromComponents.components],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PayterminalModule {}
