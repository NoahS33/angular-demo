export class Payterminal {
  _id: string
  TMS: string
  model: string
  serialnumber: string
  ipadress: string
  PSA: boolean
  boughtfromus: boolean
  condition: string
  connectiontype: string
  wifi: boolean
  fourg: string
  networkcashregister: string
  note: string
  repairs: Object
  constructor(values: Object = {}) {
    Object.assign(this, values)
  }
}
