import { Injectable } from "@angular/core"
import { from, Observable, of } from "rxjs"
import { delay, filter, map, mergeMap, take } from "rxjs/operators"
import { Payterminal } from "./payterminal.model"
import { HttpClient, HttpErrorResponse } from "@angular/common/http"
import { environment } from "src/environments/environment"

@Injectable({
  providedIn: "root"
})
export class PayterminalService {
  token: string = localStorage.getItem("token")
  constructor(private http: HttpClient) {}

  getAll(): Observable<Payterminal[]> {
    return this.http.get<any>(`${environment.api_url}/payterminals`).pipe()
  }

  getAllPayterminalsFromCustomer(customerId: string): Observable<Payterminal[]> {
    return this.http.get<any>(`${environment.api_url}/customers/${customerId}/payterminals`).pipe()
  }


  getById(customerId: string, id: string): Observable<Payterminal> {
    return this.http.get<any>(`${environment.api_url}/customers/${customerId}/payterminals/${id}`).pipe()

  }

  delete(customerId: string, id: string) {
    return this.http.delete<any>(`${environment.api_url}/customers/${customerId}/payterminals/${id}`)
  }

  update(customerId: string, payterminal: Payterminal) {
    return this.http.put<any>(
      `${environment.api_url}/customers/${customerId}/payterminals/${payterminal._id}`,
      payterminal
    )
  }
  create(customerId:string, payterminal: Payterminal) {
    return this.http.post<any>(
      `${environment.api_url}/customers/${customerId}/payterminals`,
      payterminal
    )
  }
}
