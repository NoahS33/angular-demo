import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PayterminalCreateComponent } from './payterminal-create.component';

describe('PayterminalCreateComponent', () => {
  let component: PayterminalCreateComponent;
  let fixture: ComponentFixture<PayterminalCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PayterminalCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PayterminalCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
