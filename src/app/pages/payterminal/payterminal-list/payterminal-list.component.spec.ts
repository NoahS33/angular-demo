import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PayterminalListComponent } from './payterminal-list.component';

describe('PayterminalListComponent', () => {
  let component: PayterminalListComponent;
  let fixture: ComponentFixture<PayterminalListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PayterminalListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PayterminalListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
