import { Component, OnDestroy, OnInit } from "@angular/core"
import { Router } from "@angular/router"
import { Observable, Subscription } from "rxjs"
import { Payterminal } from "../payterminal.model"
import { PayterminalService } from "../payterminal.service"

@Component({
  selector: "app-payterminal-list",
  templateUrl: "./payterminal-list.component.html",
  styleUrls: ["./payterminal-list.component.css"]
})
export class PayterminalListComponent implements OnInit, OnDestroy {
  public isAuthenticated = localStorage.getItem("isAuthenticated");
  title = "Payterminal ListComponent"
  payterminals$: Observable<Payterminal[]>
  subscribtion: Subscription

  constructor(
    private router: Router,
    private payterminalService: PayterminalService
  ) {}

  ngOnDestroy(): void {
    this.subscribtion?.unsubscribe()
  }

  ngOnInit() {
    this.payterminals$ = this.payterminalService.getAll()
  }
}
