import { Component, OnDestroy, OnInit } from "@angular/core"
import { ActivatedRoute, ParamMap, Router } from "@angular/router"
import { Customer } from "../customer.model"
import { CustomerService } from "../customer.service"
import { tap, map, switchMap, catchError } from "rxjs/operators"
import { Observable, of, Subscription } from "rxjs"

@Component({
  selector: 'app-customer-create',
  templateUrl: './customer-create.component.html',
  styleUrls: ['./customer-create.component.css']
})
export class CustomerCreateComponent implements OnInit {
  title: string
  // editMode switches between editing existing user or creating a new user.
  // Default is false (= create new user)
  editMode: boolean
  id: string
  customer: Customer
  submitted = false
  subscribtion: Subscription
  deletesubscribtion: Subscription
  customerId: string
  routeSub: Subscription

  condition = [
    { gebruikt: "Nieuw" },
    { gebruikt: "Overgezet" },
    { gebruikt: "Tweedehands" }
  ]

  constructor(
    private customerService: CustomerService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnDestroy(): void {
    this.subscribtion?.unsubscribe()
    this.deletesubscribtion?.unsubscribe()
    this.routeSub?.unsubscribe()
  }

  ngOnInit(): any {
    this.subscribtion = this.route.paramMap
    .pipe(
      tap(console.log),
      switchMap((params: ParamMap) => {

          return of (
            new Customer({

              companyname: "",
              city: "",
              street: "",
              housenumber: "",
              phonenumber: "",
              email: "",
              payterminals: []

          }))
      })

    )
    .subscribe(customer =>
        this.customer = customer
    )
  }


  onSubmit() {
    this.submitted = true

    // Save user via the service
    // Then navigate back to display view (= UserDetails).
    // The display view must then show the new or edited user.

    console.log("onSubmit: ", this.customer)

    this.customerService
      .create(this.customer)
      .pipe(tap((data) => console.log("response:", data)))
      .subscribe(
        (data) => console.log(data),
        (error) => console.error(error)
      )

    this.router.navigate([`/customers`])
  }



}
