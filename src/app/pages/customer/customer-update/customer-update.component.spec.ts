import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BehaviorSubject, of } from 'rxjs';
import { User } from '../../user/user.model';
import { Customer } from '../customer.model';
import { CustomerService } from '../customer.service';

import { CustomerUpdateComponent } from './customer-update.component';

describe('CustomerUpdateComponent', () => {
  let component: CustomerUpdateComponent;
  let fixture: ComponentFixture<CustomerUpdateComponent>;
  let routerSpy
  let authServiceSpy
  let customerServiceSpy

  const expectedCustomer: Customer = {
      _id: "5fd135fa88385b092459ff49",
        companyname: "Dönercompany",
        city: "Roosendaal",
        street: "stationstraat",
        housenumber: "7A",
        phonenumber: "0637954430",
        email: "email234@adress.com",
        payterminals: []
  };

  const expectedUserData: any = {
    _id: "id",
    isAuthenticated: true,
    token: 'some.dummy.token'

  };



  beforeEach(async () => {
    authServiceSpy= jasmine.createSpyObj('AuthService',
    [
      'login',
      'register',
      'logout'
    ])

    customerServiceSpy = jasmine.createSpyObj('WeaponService', ['getAll', 'getById', 'update'])
    const mockUser$ = new BehaviorSubject<User>(expectedUserData);
    authServiceSpy.currentUser$ = mockUser$;
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

    await TestBed.configureTestingModule({
      declarations: [ CustomerUpdateComponent ],
      providers: [
        {provide: CustomerService, useValue: customerServiceSpy}
      ]

    })

    .compileComponents();
    fixture = TestBed.createComponent(CustomerUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  afterEach(() => {
    fixture.destroy();
  });



  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('get customer failed', () => {
    customerServiceSpy.getById.and.returnValue(undefined)
    fixture.detectChanges();
    expect(component.customer).toEqual(undefined)
  });
  it('put customer', () => {
    customerServiceSpy.getById.and.returnValue(of(expectedCustomer))
    customerServiceSpy.update.and.returnValue(of(expectedCustomer))
    fixture.detectChanges();
    component.onSubmit()
    expect(customerServiceSpy.update).toHaveBeenCalled()

  });


});
