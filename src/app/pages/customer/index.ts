import { CustomerListComponent } from "../customer/customer-list/customer-list.component"
import { CustomerDetailComponent } from "./customer-detail/customer-detail.component"
import { CustomerCreateComponent } from "./customer-create/customer-create.component"
import { CustomerUpdateComponent } from "./customer-update/customer-update.component"

export const components: any[] = [
  CustomerListComponent,
  CustomerDetailComponent,
  CustomerCreateComponent,
  CustomerUpdateComponent
]

export * from "./customer-list/customer-list.component"
export * from "./customer-detail/customer-detail.component"
export * from "./customer-create/customer-create.component"
export * from "./customer-update/customer-update.component"
