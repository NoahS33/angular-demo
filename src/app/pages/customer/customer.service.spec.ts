import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { Customer } from './customer.model';


import { CustomerService } from './customer.service';

describe('CustomerService', () => {
  let service: CustomerService;
  let httpSpy
  let location: Location;
let router: Router;

  const expectedCustomer: Customer[] = [{
    _id: "5fd135fa88385b092459ff49",
        companyname: "Dönercompany",
        city: "Roosendaal",
        street: "stationstraat",
        housenumber: "7A",
        phonenumber: "0637954430",
        email: "email234@adress.com",
        payterminals: []
  }];

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CustomerService);

  });

  httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);

  TestBed.configureTestingModule({
    providers:[
      {provide: HttpClient, useValue: httpSpy}
    ]
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getall', () => {
    httpSpy.get.and.returnValue(of(expectedCustomer));

    const sub = service.getAll().subscribe((customers) =>{
      expect(customers).toEqual(expectedCustomer)
    })
    sub.unsubscribe()
  });

  it('getbyid', () => {
    httpSpy.get.and.returnValue(of(expectedCustomer));

    const sub = service.getById("id").subscribe((customer) =>{
      expect(customer).toEqual(expectedCustomer[0])
    })
    sub.unsubscribe()

  });

  it('create', () => {
    httpSpy.post.and.returnValue(of(expectedCustomer[0]));

    const sub = service.create(expectedCustomer[0]).subscribe((customer) =>{

      expect(customer).toEqual(expectedCustomer[0])
    })
    sub.unsubscribe()
  });
  it('update', () => {
    httpSpy.put.and.returnValue(of(expectedCustomer[0]));

    const sub = service.update(expectedCustomer[0]).subscribe((customer) =>{

      expect(customer).toEqual(expectedCustomer[0])
    })
    sub.unsubscribe()
  });
  it('delete', () => {
    httpSpy.delete.and.returnValue(of(expectedCustomer[0]));

    const sub = service.delete("id").subscribe((customer) =>{
      setTimeout(() => {
        expect(customer).toEqual(expectedCustomer[0])
      }, 200)

    })
    sub.unsubscribe()
  });







});
