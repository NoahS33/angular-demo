import { Injectable } from '@angular/core';
import { from, Observable, of } from "rxjs"
import { delay, filter, map, mergeMap, take } from "rxjs/operators"
import { Customer } from "./customer.model"
import { HttpClient, HttpErrorResponse } from "@angular/common/http"
import { environment } from "src/environments/environment"
import { Payterminal } from '../payterminal/payterminal.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  token: string = localStorage.getItem("token")
  constructor(private http: HttpClient) {}

  getAll(): Observable<Customer[]> {
    return this.http.get<any>(`${environment.api_url}/customers`)
  }

  getAllPayterminalsFromCustomer(customerId: string): Observable<Payterminal[]> {
    return this.http.get<any>(`${environment.api_url}/customers/${customerId}/payterminals`)
  }

  getById(id: string): Observable<Customer> {
    return this.getAll().pipe(

      map((customer) => customer.filter((item) => item._id == id)[0])
    )
  }

  delete(id: string) {
    return this.http.delete<any>(`${environment.api_url}/customers/${id}`)
  }

  update(customer: Customer) {
    return this.http.put<any>(
      `${environment.api_url}/customers/${customer._id}`,
      customer
    )
  }

  create(customer: Customer) {
    return this.http.post<any>(
      `${environment.api_url}/customers`,
      customer
    )
  }

}
