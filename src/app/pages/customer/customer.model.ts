export class Customer {
  _id: string
  companyname: string
  city: string
  street: string
  housenumber: string
  phonenumber: string
  email: string
  payterminals: Object
  constructor(values: Object = {}) {
    Object.assign(this, values)
  }
}
