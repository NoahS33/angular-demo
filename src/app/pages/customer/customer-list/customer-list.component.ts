import { Component, OnDestroy, OnInit } from "@angular/core"
import { Router } from "@angular/router"
import { Observable, Subscription } from "rxjs"
import { Customer } from "../customer.model"
import { CustomerService } from "../customer.service"

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit, OnDestroy {
  public isAuthenticated = localStorage.getItem("isAuthenticated");
  title = "Customer ListComponent"
  customers$: Observable<Customer[]>
  subscribtion: Subscription

  constructor(
    private router: Router,
    private customerService: CustomerService
  ) { }

  ngOnDestroy(): void {
    this.subscribtion?.unsubscribe()
  }

  ngOnInit() {
    console.log("ngOnInit in customerlist aangeroepen")
    this.customers$ = this.customerService.getAll()
  }

}
