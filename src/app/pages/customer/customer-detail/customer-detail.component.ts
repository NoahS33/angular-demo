import { Component, OnDestroy, OnInit } from "@angular/core"
import { CommonModule } from "@angular/common"
import { Router } from "@angular/router"
import { ActivatedRoute, ParamMap } from "@angular/router"
import { Observable, Subscription } from "rxjs"
import { switchMap, tap } from "rxjs/operators"
import { Customer } from "../customer.model"
import { CustomerService } from "../customer.service"
@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent implements OnInit {
  title = "Customer DetailComponent"
  customer: Customer
  subscribtion: Subscription
  deletesubscribtion: Subscription
  constructor(
    private router: Router,
    private customerService: CustomerService,
    private route: ActivatedRoute
  ) {}

  ngOnDestroy(): void {
    this.subscribtion?.unsubscribe()
    this.deletesubscribtion?.unsubscribe()
  }

   ngOnInit() {
    this.subscribtion = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.customerService.getById(params.get("id"))
        )
      )
      .subscribe((customer) => {
        this.customer = customer
        console.log(customer)
      })
  }

  delete() {
    this.deletesubscribtion = this.customerService
      .delete(this.customer._id)
      .subscribe(() => {
        this.router.navigate([".."], { relativeTo: this.route })
      })
  }

}
