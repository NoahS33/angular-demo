import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { RouterModule, Routes } from "@angular/router"
import * as fromComponents from "."
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CustomerCreateComponent } from './customer-create/customer-create.component'
import { AuthGuardGuard } from "src/app/auth/auth-guard.guard";

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    component: fromComponents.CustomerListComponent, canActivate: [AuthGuardGuard]
  },
  {
    path: "create",
    pathMatch: "full",
    component: fromComponents.CustomerCreateComponent, canActivate: [AuthGuardGuard]
  },
  {
    path: ":id",
    pathMatch: "full",
    component: fromComponents.CustomerDetailComponent, canActivate: [AuthGuardGuard]
  },
  {
    path: ":id/update",
    pathMatch: "full",
    component: fromComponents.CustomerUpdateComponent, canActivate: [AuthGuardGuard]
  }
]

@NgModule({
  declarations: [...fromComponents.components, CustomerCreateComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CustomerModule {}
