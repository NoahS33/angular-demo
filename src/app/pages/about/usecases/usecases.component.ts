import { Component, OnInit } from "@angular/core"
import { UseCase } from "../usecase.model"

@Component({
  selector: "app-about-usecases",
  templateUrl: "./usecases.component.html",
  styleUrls: ["./usecases.component.css"]
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = "niet-ingelogde gebruiker"
  readonly AUTHORIZED_USER = "ingelogde gebruiker"

  useCases: UseCase[] = [
    {
      id: "UC-01",
      name: "Inloggen",
      description: "Hiermee logt een bestaande gebruiker in.",
      scenario: [
        "Gebruiker vult email en password in en klikt op Login knop.",
        "De applicatie valideert de ingevoerde gegevens.",
        "Indien gegevens correct zijn dan redirect de applicatie naar het startscherm."
      ],
      actor: this.PLAIN_USER,
      precondition: "Geen",
      postcondition: "De actor is ingelogd"
    },
    {
      id: "UC-02",
      name: "Gebruiker aanmaken",
      description:
        "Hiermee kan een ingelogde gebruiker een nieuw account aanmaken voor een collega",
      scenario: [
        "Gebruiker navigeert naar de gebruiker aanmaken pagina.",
        "Gebruiker vult de gegevens in van het account dat hij wilt aanmaken.",
        "De applicatie controleert de ingevoerde gegevens en maakt een nieuwe gebruiker aan."
      ],
      actor: this.AUTHORIZED_USER,
      precondition: "De actor is ingelogd",
      postcondition: "Een nieuwe gebruiker is aangemaakt"
    },
    {
      id: "UC-03",
      name: "Klant aanmaken",
      description:
        "Hiermee kan een ingelogde gebruiker een nieuwe klant aanmaken.",
      scenario: [
        "Gebruiker navigeert naar het klantenoverzicht.",
        "Gebruiker klikt op de maak een nieuwe klant aan knop.",
        "Een lege klantbewerkdetailpagina wordt geopend.",
        "De gebruiker vult de gegevens van de klant in en drukt op aanmaken.",
        "Applicatie controleert de invoervelden en maakt de klant aan.",
        "Applicatie stuurt de gebruiker naar de detailpagina van de net aangemaakte klant"
      ],
      actor: this.AUTHORIZED_USER,
      precondition: "De actor is ingelogd",
      postcondition: "Het overzicht van klanten wordt getoond op het scherm"
    },
    {
      id: "UC-04",
      name: "Lijst klanten opvragen",
      description:
        "Hiermee kan een ingelogde gebruiker een overzicht opvragen van alle klanten",
      scenario: [
        "Gebruiker navigeert naar het klantenoverzicht.",
        "Applicatie geeft alle klanten."
      ],
      actor: this.AUTHORIZED_USER,
      precondition: "De actor is ingelogd",
      postcondition: "Het overzicht van klanten wordt getoond op het scherm"
    },
    {
      id: "UC-05",
      name: "Lijst klanten filteren",
      description:
        "Hiermee kan een ingelogde gebruiker een overzicht opvragen van alle klanten en deze filteren op pinautomaat en of servicecontract.",
      scenario: [
        "Gebruiker navigeert naar het klantenoverzicht.",
        "Applicatie geeft alle klanten.",
        "Gebruiker kiest een filter.",
        "Applicatie toont alle klanten die voldoen aan de filters."
      ],
      actor: this.AUTHORIZED_USER,
      precondition: "De actor is ingelogd",
      postcondition:
        "Het gefilterde overzicht van klanten wordt getoond op het scherm"
    },
    {
      id: "UC-06",
      name: "Details klanten inzien",
      description:
        "Hiermee kan een ingelogde gebruiker de gegevens van een klant opvragen",
      scenario: [
        "Gebruiker klikt op een klant uit de lijst met klanten",
        "Applicatie geeft alle informatie over deze klant in een detailpagina."
      ],
      actor: this.AUTHORIZED_USER,
      precondition:
        "Het overzicht met klanten is geladen.  Klant is aangemaakt.",
      postcondition: "De detailpagina van een klant wordt getoond."
    },
    {
      id: "UC-07",
      name: "Klant bewerken",
      description:
        "Hiermee kan een ingelogde gebruiker de gegevens van een klant aanpassen",
      scenario: [
        "Gebruiker klikt op de bewerken knop",
        "Applicatie opent de klantdetailbewerkpagina."
      ],
      actor: this.AUTHORIZED_USER,
      precondition:
        "De detailpagina van de klant is geladen. Klant is aangemaakt.",
      postcondition: "De gegevens van een klant zijn aangepast."
    },
    {
      id: "UC-08",
      name: "Klant verwijderen",
      description: "Hiermee kan een ingelogde gebruiker een klant verwijderen",
      scenario: [
        "Gebruiker klikt op de verwijderen knop",
        "Applicatie verwijdert de klant."
      ],
      actor: this.AUTHORIZED_USER,
      precondition:
        "De detailpagina van de klant is geladen. Klant is aangemaakt.",
      postcondition: "De klant is verwijderd."
    },
    {
      id: "UC-09",
      name: "Pinautomaat toevoegen",
      description:
        "Hiermee kan een ingelogde gebruiker een pinautomaat toevoegen aan een klant",
      scenario: [
        "Gebruiker klikt op de voeg een pinautomaat toe knop op de klantdetailpagina.",
        "Gebruiker komt op de pinautomaat toevoeg pagina en voert hier de gegevens van de pinautomaat in.",
        "Applicatie controleert de invoervelden en maakt de pinautomaat aan."
      ],
      actor: this.AUTHORIZED_USER,
      precondition:
        "De detailpagina van de klant is geladen. Klant is aangemaakt.",
      postcondition: "Een pinautomaat is aangemaakt en toegevoegd aan de klant."
    },
    {
      id: "UC-10",
      name: "Pinautomaat gegevens inzien",
      description:
        "Hiermee kan een ingelogde gebruiker de details van een pinautomaat bekijken",
      scenario: [
        "Gebruiker klikt op een pinautomaat in de lijst van pinautomaten die een klant heeft op de klantdetailpagina.",
        "Gebruiker komt op de pinautomaatdetailpagina."
      ],
      actor: this.AUTHORIZED_USER,
      precondition:
        "De detailpagina van de klant is geladen. Klant is aangemaakt.",
      postcondition: "Een pinautomaat is aangemaakt en toegevoegd aan de klant."
    },
    {
      id: "UC-11",
      name: "Pinautomaat bewerken",
      description:
        "Hiermee kan een ingelogde gebruiker een pinautomaat bewerken",
      scenario: [
        "Gebruiker klikt op de pinautomaat in de lijst van pinautomaten die een klant heeft op de klantdetailpagina.",
        "Gebruiker komt op de pinautomaatdetailpagina en klikt hier op bewerken.",
        "Gebruiker past gegevens aan, en klikt op aanpassen.",
        "Applicatie controleert de invoervelden en past de pinautomaat aan."
      ],
      actor: this.AUTHORIZED_USER,
      precondition:
        "De detailpagina van de klant is geladen. Klant is aangemaakt. Pinautomaat is aangemaakt.",
      postcondition: "Een pinautomaat is aangepast."
    },
    {
      id: "UC-12",
      name: "Pinautomaat verwijderen",
      description:
        "Hiermee kan een ingelogde gebruiker een pinautomaat verwijderen",
      scenario: [
        "Gebruiker klikt op de pinautomaat in de lijst van pinautomaten die een klant heeft op de klantdetailpagina.",
        "Gebruiker komt op de pinautomaatdetailpagina en klikt hier op verwijderen.",
        "Applicatie geeft een pop-up weer: Zeker weten?",
        "Gebruiker klikt op bevestigen.",
        "Applicatie controleert de invoervelden en past de pinautomaat aan."
      ],
      actor: this.AUTHORIZED_USER,
      precondition:
        "De detailpagina van de klant is geladen. Klant is aangemaakt. Pinautomaat is aangemaakt.",
      postcondition: "De pinautomaat is verwijderd."
    },
    {
      id: "UC-13",
      name: "Reparatie toevoegen",
      description:
        "Hiermee kan een ingelogde gebruiker een reparatie toevoegen aan een klant en een pinautomaat",
      scenario: [
        "Gebruiker klikt op de voeg een reparatie toe knop op de klantdetailpagina.",
        "Gebruiker komt op de reparatie toevoeg pagina en voert hier de gegevens van de reparatie in.",
        "Applicatie controleert de invoervelden en maakt de reparatie aan.",
        "De gebruiker wordt gestuurd naar de reparatiedetailpagina."
      ],
      actor: this.AUTHORIZED_USER,
      precondition:
        "De detailpagina van de klant is geladen. Klant is aangemaakt.",
      postcondition: "Een reparatie is aangemaakt en toegevoegd aan de klant."
    },
    {
      id: "UC-14",
      name: "Reparatie bewerken",
      description: "Hiermee kan een ingelogde gebruiker een reparatie bewerken",
      scenario: [
        "Gebruiker klikt op de reparatie in de lijst van reparaties bij een klant.",
        "Gebruiker komt op de reparatiedetailpagina en klikt hier op bewerken.",
        "Gebruiker komt op de reparatiedetailbewerkpagina en past hier gegevens aan.",
        "Applicatie controleert de invoervelden en past de reparatie aan.",
        "De gebruiker wordt gestuurd naar de reparatiedetailpagina."
      ],
      actor: this.AUTHORIZED_USER,
      precondition:
        "De detailpagina van de klant is geladen. Klant is aangemaakt. Reparatie is aangemaakt.",
      postcondition: "Een reparatie is aangepast."
    },
    {
      id: "UC-15",
      name: "Reparatie verwijderen",
      description:
        "Hiermee kan een ingelogde gebruiker een reparatie verwijderen.",
      scenario: [
        "Gebruiker klikt op de reparatie in de lijst van reparaties bij een klant.",
        "Gebruiker komt op de reparatiedetailpagina en klikt hier op verwijderen.",
        "Applicatie verwijdert de reparatie.",
        "De gebruiker wordt gestuurd naar de klantdetailpagina."
      ],
      actor: this.AUTHORIZED_USER,
      precondition:
        "De detailpagina van de klant is geladen. Klant is aangemaakt. Reparatie is aangemaakt.",
      postcondition: "Een reparatie is verwijderd."
    }
  ]

  constructor() {}

  ngOnInit(): void {}
}
