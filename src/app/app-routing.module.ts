import { NgModule } from "@angular/core"
import { Routes, RouterModule } from "@angular/router"
import { AuthGuardGuard } from "./auth/auth-guard.guard"
import { LoginComponent } from "./auth/login/login.component"
import { RegisterComponent } from "./auth/register/register.component"
import { LayoutComponent } from "./layout/layout.component"
import { UsecasesComponent } from "./pages/about/usecases/usecases.component"
import { DashboardComponent } from "./pages/dashboard/dashboard.component"

const routes: Routes = [
  {
    path: "",
    component: LayoutComponent,
    children: [
      { path: "", pathMatch: "full", redirectTo: "dashboard" },
      { path: "dashboard", pathMatch: "full", component: DashboardComponent, canActivate: [AuthGuardGuard]},
      { path: "about", pathMatch: "full", component: UsecasesComponent, canActivate: [AuthGuardGuard] },
      {
        path: "customers",
        loadChildren: () =>
          import("./pages/customer/customer.module").then(
            (m) => m.CustomerModule
          )
      },
      {
        path: "customers/:customerId/payterminals",
        loadChildren: () =>
          import("./pages/payterminal/payterminal.module").then(
            (m) => m.PayterminalModule
          )
      },
      {
        path: "customers/:customerId/payterminals/:payterminalId/repairs",
        loadChildren: () =>
          import("./pages/repair/repair.module").then(
            (m) => m.RepairModule
          )
      },
      {
        path: "users",
        loadChildren: () =>
          import("./pages/user/user.module").then((m) => m.UserModule)
      }
    ]
  },
  { path: "login", pathMatch: "full", component: LoginComponent },
  { path: "register", pathMatch: "full", component: RegisterComponent },
  { path: "**", pathMatch: "full", redirectTo: "dashboard" }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
