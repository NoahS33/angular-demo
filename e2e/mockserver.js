const express = require("express")
const port = 3000;

let app = express();
let routes = require("express").Router();

const cors = require('cors');
app.use(cors());

// app.use(function (req, res, next) {
//   res.setHeader("Access-Control-Allow-Origin", "*");
//   res.setHeader(
//     "Access-Control-Allow-Methods",
//     "GET, POST, OPTIONS, PUT, PATCH, DELETE"
//   );
//   res.setHeader(
//     "Access-Control-Allow-Headers",
//     "X-Requested-With,content-type"
//   );
//   res.setHeader("Access-Control-Allow-Credentials", true);
//   next();
// });
routes.post("/login", (req, res, next) => {
  console.log("Mockserver: Login is aangeroepen.")
  res.status(200).json({token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6I…AzOX0.6mvovghjKHWRUBglNDtc2oXFhZNb3TLCZeBnZGBUDHI"});
});

routes.get("/customers", (req, res, next) => {
  console.log("Mockserver: Get Customers is aangeroepen.")
  res.status(200).json([
    {
        "_id": "5fd133351cd28e1c903a8013",
        "companyname": "updated",
        "city": "test",
        "street": "test",
        "housenumber": "test",
        "phonenumber": "0612121212",
        "email": "email@adres.com",
        "payterminals": [
            {
                "_id": "5fc4dba78f434d1328b5c53d",
                "TMS": 34835435,
                "model": "V650",
                "serialnumber": "38495834nf3",
                "ipadress": "192.168.256.1",
                "PSA": true,
                "boughtFromUs": true,
                "condition": "Nieuw",
                "connectiontype": "ITS",
                "wifi": true,
                "fourg": false,
                "networkcashregister": true,
                "note": "Dit is de Geüpdatete versie van dit pinautomaat.",
                "repairs": []
            },
            {
                "_id": "5fc55c5f6f194d001712ddea",
                "TMS": 34835435,
                "model": "V450",
                "serialnumber": "38523dfdf",
                "ipadress": "192.168.256.1",
                "PSA": false,
                "boughtFromUs": true,
                "condition": "Nieuw",
                "connectiontype": "Serieel",
                "wifi": true,
                "fourg": true,
                "networkcashregister": true,
                "note": "test",
                "__v": 0,
                "repairs": []
            },
            {
                "_id": "5ff1f1b6e7b9612c84565707",
                "TMS": 34835435,
                "model": "V400",
                "serialnumber": "63583785734",
                "ipadress": "192.168.172.1",
                "PSA": false,
                "boughtFromUs": false,
                "condition": "Nieuw",
                "connectiontype": "ITS",
                "wifi": true,
                "fourg": false,
                "networkcashregister": true,
                "note": "test",
                "repairs": [
                    {
                        "_id": "6071f0a95c48dd0e68ce52ec",
                        "title": "Gebarsten scherm gerepareerd",
                        "description": "Door het scherm te vervangen is de pinautomaat weer te gebruiken.",
                        "date": "2020-07-12T00:00:00.000Z"
                    }
                ]
            },
            {
                "_id": "6071f1125c48dd0e68ce52ed",
                "TMS": 34835435,
                "model": "V400",
                "serialnumber": "63583785734",
                "ipadress": "192.168.172.1",
                "PSA": false,
                "boughtFromUs": false,
                "condition": "Nieuw",
                "connectiontype": "ITS",
                "wifi": true,
                "fourg": false,
                "networkcashregister": true,
                "note": "test",
                "repairs": []
            }
        ],
        "__v": 8
    },
    {
        "_id": "5fd135fa88385b092459ff49",
        "companyname": "Dönercompany",
        "city": "Roosendaal",
        "street": "stationstraat",
        "housenumber": "7A",
        "phonenumber": "0637954430",
        "email": "email23@adress.com",
        "payterminals": [
            {
                "_id": "5fcdf1c6f772a45fc0e114c8",
                "TMS": 34835435,
                "model": "V400",
                "serialnumber": "63583785734",
                "ipadress": "192.168.172.1",
                "PSA": false,
                "boughtFromUs": false,
                "condition": "Nieuw",
                "connectiontype": "ITS",
                "wifi": true,
                "fourg": false,
                "networkcashregister": true,
                "note": "test",
                "repairs": [
                    {
                        "_id": "5fcdfbf1f772a45fc0e114ca",
                        "title": "Waterschade opgelost",
                        "description": "Door het moederbord te vervangen is de vuurschade opgelost.",
                        "date": "2020-07-12T00:00:00.000Z"
                    },
                    {
                        "_id": "6076c84d077c473ea821e676",
                        "title": "Scherm vervangen",
                        "description": "De pinautomaat was gevallen en het scherm was gebarsten, dus we hebben er een nieuw scherm op gezet.",
                        "date": null
                    }
                ],
                "__v": 0
            },
            {
                "_id": "5fd1360888385b092459ff4d",
                "TMS": 34835435,
                "model": "V400",
                "serialnumber": "63583785734",
                "ipadress": "192.168.172.1",
                "PSA": false,
                "boughtFromUs": false,
                "condition": "Nieuw",
                "connectiontype": "ITS",
                "wifi": true,
                "fourg": false,
                "networkcashregister": true,
                "note": "test",
                "repairs": [
                    {
                        "_id": "5fd1360888385b092459ff4e",
                        "title": "Waterschade opgelost",
                        "description": "Door het moederbord te vervangen is de waterschade opgelost.",
                        "date": "2020-07-12T00:00:00.000Z"
                    }
                ]
            },
            {
                "_id": "5fedb77b6c04386a2cef7c55",
                "TMS": 34835435,
                "model": "V400",
                "serialnumber": "63583785734",
                "ipadress": "192.168.172.1",
                "PSA": true,
                "boughtFromUs": false,
                "condition": "Nieuw",
                "connectiontype": "ITS",
                "wifi": true,
                "fourg": true,
                "networkcashregister": true,
                "note": "test",
                "repairs": []
            },
            {
                "_id": "6073ffbb41c11922e05a14ce",
                "TMS": 1243535,
                "model": "V500",
                "serialnumber": "23435345634",
                "ipadress": "",
                "PSA": true,
                "condition": "Nieuw",
                "connectiontype": "Geen",
                "wifi": true,
                "fourg": false,
                "networkcashregister": false,
                "note": "",
                "repairs": []
            }
        ],
        "__v": 23
    }
]);
});

routes.get("/customers/5fd133351cd28e1c903a8013", (req, res, next) => {
  console.log("Mockserver: Get Customers by id:5fd133351cd28e1c903a8013 is aangeroepen.")
  res.status(200).json({
    "_id": "5fd133351cd28e1c903a8013",
    "companyname": "updated",
    "city": "test",
    "street": "test",
    "housenumber": "test",
    "phonenumber": "0612121212",
    "email": "email@adres.com",
    "payterminals": [

        {
            "_id": "5ff1f1b6e7b9612c84565707",
            "TMS": 34835435,
            "model": "V400",
            "serialnumber": "63583785734",
            "ipadress": "192.168.172.1",
            "PSA": false,
            "boughtFromUs": false,
            "condition": "Nieuw",
            "connectiontype": "ITS",
            "wifi": true,
            "fourg": false,
            "networkcashregister": true,
            "note": "test",
            "repairs": [
                {
                    "_id": "6071f0a95c48dd0e68ce52ec",
                    "title": "Gebarsten scherm gerepareerd",
                    "description": "Door het scherm te vervangen is de pinautomaat weer te gebruiken.",
                    "date": "2020-07-12T00:00:00.000Z"
                }
            ]
        }
    ],
    "__v": 8
})
});

routes.put('/customers/5fd133351cd28e1c903a8013', (req, res, next) => {
  console.log("Mockserver: Put Customer by id:5fd133351cd28e1c903a8013 is aangeroepen.");
  res.status(204).json();
});

app.use(routes);

app.use("", function (req, res, next) {
  next({ error: "Non-existing endpoint" });
});

app.use((err, req, res, next) => {
  res.status(400).json(err);
});

app.listen(port, () => {
  console.log("Mock backend server running on port", port);
});
