import { CustomerListPage } from './customer-list.po';
import { browser, element, logging, by, ExpectedConditions } from 'protractor';
import { protractor } from 'protractor/built/ptor'

describe('workspace-project CustomerList', () => {
  let page: CustomerListPage;

  beforeEach(() => {
    page = new CustomerListPage();
    browser.waitForAngularEnabled(false);
    page.navigateTo('/customers');
    browser.executeScript("window.localStorage.setItem('token', 'ditiseentoken');");
  });



  it('should be on the customers page', () => {
    page.navigateTo('/customers');
    expect(browser.getCurrentUrl()).toContain('/customers');
  })
  it('should show add button of customers', () =>{
    expect(page.add.isDisplayed()).toBeTruthy();
  })
  it('should show list of customers', () =>{
    expect(page.items.isDisplayed()).toBeTruthy();
  })
  it('should show a logout button', () => {
  expect(page.logout.isDisplayed()).toBeTruthy();
  })
  it('should show a name column', () => {
    expect(page.nameColumn.isDisplayed()).toBeTruthy();
  })
  it('should show a company in the list', () => {
    expect(element.all(by.tagName('tr')).get(0).isDisplayed()).toBeTruthy();
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
})
