import { browser, element, logging, by, ExpectedConditions } from 'protractor';
import { protractor } from 'protractor/built/ptor'
import { CustomerUpdatePage } from './customer-update.po';

describe('workspace-project CustomerUpdatePage', () => {
  let page: CustomerUpdatePage;

  beforeEach(() => {
    page = new CustomerUpdatePage();
    browser.waitForAngularEnabled(false);
    page.navigateTo('/customers');
    page.navigateTo('/customers/5fd133351cd28e1c903a8013/update');
    browser.executeScript("window.localStorage.setItem('token', 'ditiseentoken');");
  });

  it('should be on the customerUpdatePage', () => {
    page.navigateTo('/customers')
    page.navigateTo('/customers/5fd133351cd28e1c903a8013/update');
    expect(browser.getCurrentUrl()).toContain('/customers/5fd133351cd28e1c903a8013');
  })

  it('page should be loaded and all elements should be displayed.', () => {
    page.navigateTo('/customers')
    page.navigateTo('/customers/5fd133351cd28e1c903a8013/update');
    expect(browser.getCurrentUrl()).toContain('/customers/5fd133351cd28e1c903a8013/update');
    expect(page.inputCompanyname.isDisplayed()).toBeTruthy()
    expect(page.inputCity.isDisplayed()).toBeTruthy
    expect(page.inputStreet.isDisplayed()).toBeTruthy
    expect(page.inputHousenumber.isDisplayed()).toBeTruthy
    expect(page.inputPhonenumber.isDisplayed()).toBeTruthy
    expect(page.inputEmail.isDisplayed()).toBeTruthy
    expect(page.submitEditCustomer.isDisplayed()).toBeTruthy
  })

  it('values should be loaded into the input fields', () => {
    page.navigateTo('/customers')
    page.navigateTo('/customers/5fd133351cd28e1c903a8013/update');
    expect(browser.getCurrentUrl()).toContain('/customers/5fd133351cd28e1c903a8013/update');
    browser.driver.sleep(500);
    expect(page.inputCompanyname.isDisplayed()).toBeTruthy()
    expect(page.inputCompanyname.getAttribute('value')).toEqual("updated")
  })

  it('should update the customer when changing the name', () => {
    page.navigateTo('/customers')
    page.navigateTo('/customers/5fd133351cd28e1c903a8013/update');
    expect(browser.getCurrentUrl()).toContain('/customers/5fd133351cd28e1c903a8013/update');
    expect(page.inputCompanyname.isDisplayed()).toBeTruthy();
    expect(page.inputCompanyname.getAttribute('value')).toEqual("updated");
    page.inputCompanyname.click();
    page.inputCompanyname.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
    page.inputCompanyname.sendKeys(protractor.Key.BACK_SPACE);
    page.inputCompanyname.sendKeys('ditisdenieuweNaam');
    page.submitEditCustomer.click()
    expect(browser.getCurrentUrl()).toContain('/customers');
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
})
