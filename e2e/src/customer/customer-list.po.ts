import { browser, by, element, ElementFinder } from 'protractor';

export class CustomerListPage {
  navigateTo(string): Promise<unknown> {
    return browser.get(browser.baseUrl + string) as Promise<unknown>;
  }

  getTitleText(): Promise<string> {
    return element(by.id('title')).getText() as Promise<string>;
  }
  get items(): ElementFinder {
    return element(by.id('tableCustomers')) as ElementFinder;
  }
  get add(): ElementFinder {
    return element(by.id('addCustomer')) as ElementFinder;
  }
  get logout(): ElementFinder {
    return element(by.id('logout')) as ElementFinder;
  }
  get nameColumn(): ElementFinder {
    return element(by.id('NameColumn')) as ElementFinder;
  }
  get detailButton(): ElementFinder {
    return element(by.id('DetailButton')) as ElementFinder;
  }

  // get listCompanyName(): ElementFinder {
  //   return element(by.id('ListCompanyName')) as ElementFinder;
  // }

}
