import { browser, by, element, ElementFinder } from 'protractor';

export class CustomerUpdatePage {
  navigateTo(string): Promise<unknown> {
    return browser.get(browser.baseUrl + string) as Promise<unknown>;
  }

  getTitleText(): Promise<string> {
    return element(by.id('title')).getText() as Promise<string>;
  }
  get DetailButton(): ElementFinder {
    return element(by.id('DetailButton')) as ElementFinder;
  }

  get editCustomer(): ElementFinder {
    return element(by.id('EditCustomer')) as ElementFinder;
  }

  get inputCompanyname(): ElementFinder {
    return element(by.name('companyname')) as ElementFinder;
  }

  get inputCity(): ElementFinder {
    return element(by.id('InputCity')) as ElementFinder;
  }

  get inputStreet(): ElementFinder {
    return element(by.id('InputStreet')) as ElementFinder;
  }
  get inputHousenumber(): ElementFinder {
    return element(by.id('InputHousenumber')) as ElementFinder;
  }
  get inputPhonenumber(): ElementFinder {
    return element(by.id('InputPhonenumber')) as ElementFinder;
  }
  get inputEmail(): ElementFinder {
    return element(by.id('InputEmail')) as ElementFinder;
  }
  get submitEditCustomer(): ElementFinder {
    return element(by.id('submitEditCustomer')) as ElementFinder;
  }




}
